# Deploy and Manage Gitlab Runners on Amazon EC2

This solution based on the [blog post](https://aws.amazon.com/blogs/devops/deploy-and-manage-gitlab-runners-on-amazon-ec2/) 
automates Gitlab Runner deployment and administrative tasks on Amazon EC2 through Infrastructure-as-Code (IaC).

A few changes were done:

- latest AMI image is used automatically using SSM property /aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2
- update of deploy-runner.sh to automatically handle S3 bucket and retrieve gitlab registration token from SSM
- Runner registration process in gitlab-runner.yaml was updated to match the latest version of Gitlab of code.europa.eu

## Notes
Gitlab recently updated how Gitlab Runners register to Gitlab. A runner (with configuration) first needs to be created in the Gitlab UI.
This generates an authentication token that must be provided when registering the runner to the Gitlab instance.

See https://gitlab.com/gitlab-org/gitlab/-/issues/387993 and https://docs.gitlab.com/ee/ci/runners/new_creation_workflow.html for more information.

A runner can either be linked to a single project or be shared. With docker images, it is beneficial for the runners to be shared: https://code.europa.eu/help/ci/runners/runners_scope.md#shared-runners

## Overview of the solution
The following diagram displays the solution architecture. 
It also shows the workflow of deploying the Gitlab Runner and registering it to Gitlab projects. 
<img src="Images/gitlab-runner-architecture.png" width="700">

1. We use AWS CloudFormation to describe the infrastructure that is hosting the Gitlab Runner. The user runs a deployment script in order to deploy the CloudFormation template. The template is parameterized, and the parameters are defined in a properties file. The properties file specifies the infrastructure configuration, as well as the environment in which to deploy the template.
2. The deployment script calls CloudFormation CreateStack API to create a Gitlab Runner stack in the specified environment.
3. During stack creation, an EC2 autoscaling group is created with the desired number of EC2 instances. Each instance is launched via a launch template, which is created with values from the properties file. An IAM role is created and attached to the EC2 instance. The role contains permissions required for the Gitlab Runner to execute pipeline jobs. A lifecycle hook is attached to the autoscaling group on instance termination events. This ensures graceful instance termination.
4. During instance launch, CloudFormation uses a cfn-init helper script to install and configure the Gitlab Runner:
   1. (4a) cfn-init installs the Gitlab Runner software on the EC2 instance. 
   2. (4b) cfn-init configures the Gitlab Runner as a docker executor. The docker executor implementation lets the Gitlab Runner run each build in a separate and isolated container. The docker image contains the software required to run the pipeline workloads, thereby eliminating the need to install these packages during each build. 
   3. (4c) cfn-init registers the Gitlab Runner to Gitlab projects specified in the properties file, so that these projects can utilize the Gitlab Runner to run pipelines.
5. The user may repeat the same steps to deploy Gitlab Runner into another environment. 

## Prerequisites
For this walkthrough, you need the following: 
- A Gitlab account on https://code.europa.eu with permissions to create a runner (and receive an authentication token).
- In your AWS account, add this group registration token in AWS System Manager Parameter Store and mention the path in the properties file (e.g. SSM path /dev/gitlab-registration-token)
- A Gitlab Container Registry.
- A technical IAM user with credentials properly configured (typically under ~/.aws/credentials) with these permissions (admin in Srv4dev):
    - AmazonEC2FullAccess
    - AutoScalingFullAccess
    - AmazonS3FullAccess
    - AmazonSSMFullAccess
    - AmazonEventBridgeFullAccess
    - AWSCloudFormationFullAccess
    - AWSLambda_FullAccess
    - IAMFullAccess
    - AmazonECS_FullAccess
    - AmazonEC2ContainerRegistryPowerUser
- Nodejs and npm installed on the localhost/laptop. 
- A VPC with 2 private subnets and that is connected to the internet via NAT gateway allowing outbound traffic (provided by Srv4dev).
- The following IAM service-linked role created in the AWS account: AWSServiceRoleForAutoScaling
- An S3 bucket for storing Lambda deployment packages.

## Deploy the Gitlab Runner stack
The Gitlab Runner infrastructure is described in the Cloudformation template gitlab-runner.yaml. Its configuration is stored in a properties file called dev.properties (for development environment). A launch template is created with the values in the properties file. Then it is used to launch instances. This architecture lets you deploy Gitlab Runner to as many environments as you like by utilizing the configurations provided in the appropriate properties files.

### To deploy the Gitlab Runner stack:
1.	Ask the owner of the Gitlab group to create a shared runner and provide the authentication token generated. This ensures that all projects of the group can use the runner for CI/CD.
2.	Update the dev.properties file parameters according to your own environment. Refer to the gitlab-runner.yaml file for a description of these parameters. You may also create an additional properties file for deploying into other environments. 
3.	Run the deploy script to deploy the runner: 
```
cd <your-repo-dir>
./deploy-runner.sh <properties-file> <region> <aws-profile> <stack-name> 

<properties-file> is the name of the properties file. 
<region> is the region where you want to deploy the stack. 
<aws-profile> is the name of the CLI profile you set up in the prerequisites section. 
<stack-name> is the name you chose for the CloudFormation stack. 
```
For example: 
```
./deploy-runner.sh dev.properties eu-west-1 tedai-dev d-ew1-gitlab-runner
```

After the stack is deployed successfully, you will see the Gitlab Runner autoscaling group created in the EC2 console. 

<img src="Images/gitlab-runner-demo-asg.png" width="700">

Now go to your Gitlab project Settings->CICD->Runners->Available specific runners, you will see the fully configured Gitlab Runner. The green circle indicates that the Gitlab Runner is ready for use. 

<img src="Images/available-runners-1.png" width="500">

## Updating the Gitlab Runner
To update the stack, update the properties file or the gitlab-runner.yaml and execute again the deployment script.

For example: 
```
./deploy-runner.sh dev.properties eu-west-1 tedai-dev d-ew1-gitlab-runner
```

## Terminate the Gitlab Runner
There are times when an autoscaling group instance must be terminated. For example, during an autoscaling scale-in event, or when the instance is being replaced by a new instance during a stack update, as seen previously. When terminating an instance, you must ensure that the Gitlab Runner finishes executing any running jobs before the instance is terminated, otherwise your environment could be left in an inconsistent state. Also, we want to ensure that the terminated Gitlab Runner is removed from the Gitlab project. We utilize an autoscaling lifecycle hook to achieve these goals.  

<img src="Images/gitlab-runner-lifecycle-hook.png" width="700">

The lifecycle hook works like this: A CloudWatch event rule actively listens for the EC2 Instance-terminate events. When one is detected, the event rule triggers a Lambda function. The Lambda function calls SSM Run Command to run a series of commands on the EC2 instances, via a SSM Document. The commands include stopping the Gitlab Runner gracefully when all running jobs are finished, de-registering the runner from Gitlab projects, and signaling the autoscaling group to terminate the instance.  

There are also times when you want to terminate an instance manually. For example, when an instance is suspected to not be functioning properly. To terminate an instance from the Gitlab Runner autoscaling group, use the following command:

```
aws autoscaling terminate-instance-in-auto-scaling-group \
    --instance-id="${InstanceId}" \
    --no-should-decrement-desired-capacity \
    --region="${region}" \
    --profile="${profile}"
```
The above command terminates the instance. The lifecycle hook ensures that the cleanup steps are conducted properly, and the autoscaling group launches another new instance to replace the old one.  

Note that if you terminate the instance by using the "ec2 terminate-instance" command, then the autoscaling lifecycle hook actions will not be triggered.

## Autoscale the runner based on custom performance metrics
Each Gitlab Runner can be configured to handle a fixed number of [concurrent jobs](https://docs.gitlab.com/runner/configuration/advanced-configuration.html). Once this capacity is reached for every runner, any new jobs will be in a Queued/Waiting status until the current jobs complete, which would be a poor experience for our team. Setting the number of concurrent jobs too high on our runners would also result in a poor experience, because all jobs leverage the same CPU, memory, and storage in order to conduct the builds.

In this solution, we utilize a scheduled Lambda function that runs every minute in order to inspect the number of jobs running on every runner, leveraging the [Prometheus Metrics](https://docs.gitlab.com/ee/administration/monitoring/prometheus/gitlab_metrics.html#gitlab-prometheus-metrics) endpoint that the runners expose. If we approach the concurrent build limit of the group, then we increase the Autoscaling Group size so that it can take on more work. As the number of concurrent jobs decreases, then the scheduled Lambda function will scale the Autoscaling Group back in an effort to minimize cost. The Scaling-Up operation will ignore the Autoscaling Group’s cooldown period, which will help ensure that our team is not waiting on a new instance, whereas the Scale-Down operation will obey the group’s cooldown period.

Here is the logical sequence diagram for the work:

<img src="Images/gitlab-runner-autoscaling.png" width="700">

For operational monitoring, the Lambda function also publishes custom CloudWatch Metrics for the count of active jobs, along with the target and actual capacities of the Autoscaling group. We can utilize this information to validate that the system is working properly and determine if we need to modify any of our autoscaling parameters.

<img src="Images/example-metrics.png" width="700">

## Troubleshooting
Problem: I deployed the CloudFormation template, but no runner is listed in my repository. 

Possible Cause: Errors have been encountered during cfn-init, causing runner registration to fail. Connect to your runner EC2 instance, and check /var/log/cfn-*.log files. 

## TODO
- Currently, this setup only supports one environment (dev), some files must be adapted to support other environments (bucket-policy.json), create a new properties file, ...

# License

This library is licensed under the MIT-0 License. See the LICENSE file.

